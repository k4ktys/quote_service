package entity

import (
	"time"

	"gitlab.com/golight/dao/types"
)

type UserDTO struct {
	Login        string `json:"login"`
	PasswordHash string `json:"password_hash"`
}

//go:generate genstorage -filename=$GOFILE
type User struct {
	ID           int            `json:"-" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null" db_ops:"id"`
	Login        string         `json:"-" db:"login" db_ops:"create,conflict,upsert" db_type:"varchar(144)" db_default:"not null" db_index:"index,unique"`
	PasswordHash string         `json:"-" db:"password_hash" db_ops:"create,update,upsert" db_type:"varchar(144)" db_default:"not null"`
	CreatedAt    time.Time      `json:"-" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	UpdatedAt    time.Time      `json:"-" db:"updated_at" db_ops:"update" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	DeletedAt    types.NullTime `json:"-" db:"deleted_at" db_ops:"update" db_type:"timestamp" db_default:"default null" db_index:"index"`
}

func (u *User) TableName() string {
	return "users"
}

func (u *User) OnCreate() []string {
	return []string{}
}

func (u *User) FieldsPointers() []interface{} {
	return []interface{}{
		&u.ID,
		&u.Login,
		&u.PasswordHash,
		&u.CreatedAt,
		&u.UpdatedAt,
		&u.DeletedAt,
	}
}
