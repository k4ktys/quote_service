package repository

import (
	"context"
	"fmt"

	"gitlab.com/golight/dao"
	"gitlab.com/golight/dao/params"
	"gitlab.com/golight/dao/types"
	"gitlab.com/k4ktys/quote_service/modules/user/entity"

	"time"
)

type UsersStorage struct {
	adapter dao.DAOFace
}

func NewUsersStorage(sqlAdapter dao.DAOFace) UsersStorage {
	return UsersStorage{adapter: sqlAdapter}
}

func (u *UsersStorage) Create(ctx context.Context, dto entity.User) (int64, error) {
	return u.adapter.Create(ctx, &dto)
}

func (u *UsersStorage) Update(ctx context.Context, dto entity.User) error {
	return u.adapter.Update(
		ctx,
		&dto,
		params.Condition{
			Equal: map[string]interface{}{"id": dto.ID},
		},
		params.Update,
	)
}

func (u *UsersStorage) GetByID(ctx context.Context, usersID int) (entity.User, error) {
	var list []entity.User
	var table entity.User
	err := u.adapter.List(ctx, &list, &table, params.Condition{
		Equal: map[string]interface{}{"id": usersID},
	})
	if err != nil {
		return entity.User{}, err
	}
	if len(list) < 1 {
		return entity.User{}, fmt.Errorf("users storage: GetByID not found")
	}
	return list[0], err
}

func (u *UsersStorage) GetList(ctx context.Context, condition params.Condition) ([]entity.User, error) {
	var list []entity.User
	var table entity.User
	err := u.adapter.List(ctx, &list, &table, condition)
	if err != nil {
		return nil, err
	}

	return list, nil
}

func (u *UsersStorage) Delete(ctx context.Context, usersID int) error {
	table, err := u.GetByID(ctx, usersID)
	if err != nil {
		return err
	}

	table.DeletedAt = types.NewNullTime(time.Now())

	return u.adapter.Update(
		ctx,
		&table,
		params.Condition{
			Equal: map[string]interface{}{"id": table.ID},
		},
		params.Update,
	)
}
