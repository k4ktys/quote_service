package handler

import (
	"net/http"

	"gitlab.com/golight/responder"
	"gitlab.com/k4ktys/quote_service/modules/user/service"
)

type UserHandlerer interface {
	Update(w http.ResponseWriter, r *http.Request)
	GetByID(w http.ResponseWriter, r *http.Request)
	Delete(w http.ResponseWriter, r *http.Request)
	GetByLogin(w http.ResponseWriter, r *http.Request)
}

type UserHandler struct {
	responder responder.Responder
	service   service.UserServicer
}

func NewUserHandler(r responder.Responder, s service.UserServicer) UserHandlerer {
	return &UserHandler{
		responder: r,
		service:   s,
	}
}

func (u *UserHandler) Delete(w http.ResponseWriter, r *http.Request) {
	panic("unimplemented")
}

func (u *UserHandler) GetByID(w http.ResponseWriter, r *http.Request) {
	panic("unimplemented")
}

func (u *UserHandler) GetByLogin(w http.ResponseWriter, r *http.Request) {
	panic("unimplemented")
}

func (u *UserHandler) Update(w http.ResponseWriter, r *http.Request) {
	panic("unimplemented")
}
