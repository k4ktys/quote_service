package router

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/k4ktys/quote_service/modules/user/handler"
)

func UserRouter(handler handler.UserHandlerer) http.Handler {
	r := chi.NewRouter()

	r.Post("/delete", handler.Delete)
	r.Post("/get_by_id", handler.GetByID)
	r.Post("/get_by_login", handler.GetByLogin)
	r.Post("/update", handler.Update)

	return r
}
