package service

import (
	"context"
	"fmt"

	"gitlab.com/golight/dao/params"
	"gitlab.com/k4ktys/quote_service/modules/user/entity"
	"gitlab.com/k4ktys/quote_service/modules/user/repository"
)

type UserServicer interface {
	Create(ctx context.Context, user entity.UserDTO) (int64, error)
	Update(ctx context.Context, dto entity.User) error
	GetByID(ctx context.Context, usersID int) (entity.User, error)
	Delete(ctx context.Context, usersID int) error
	GetByLogin(ctx context.Context, login string) (entity.UserDTO, error)
}

type UserService struct {
	repository repository.Userser
}

func NewUserService(r repository.Userser) UserServicer {
	return &UserService{repository: r}
}

func (u *UserService) Create(ctx context.Context, user entity.UserDTO) (int64, error) {
	return u.repository.Create(ctx, entity.User{
		Login:        user.Login,
		PasswordHash: user.PasswordHash,
	})
}

func (u *UserService) GetByLogin(ctx context.Context, login string) (entity.UserDTO, error) {
	condition := map[string]interface{}{
		"login": login,
	}

	users, err := u.repository.GetList(ctx, params.Condition{Equal: condition})

	if err != nil {
		return entity.UserDTO{}, err
	} else if users == nil {
		return entity.UserDTO{}, fmt.Errorf("user not found")
	} else {
		return entity.UserDTO{
			Login:        users[0].Login,
			PasswordHash: users[0].PasswordHash,
		}, nil
	}
}

func (u *UserService) Delete(ctx context.Context, usersID int) error {
	return u.repository.Delete(ctx, usersID)
}

func (u *UserService) GetByID(ctx context.Context, usersID int) (entity.User, error) {
	return u.repository.GetByID(ctx, usersID)
}

func (u *UserService) Update(ctx context.Context, dto entity.User) error {
	return u.repository.Update(ctx, dto)
}
