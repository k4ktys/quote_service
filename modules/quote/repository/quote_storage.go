package repository

import (
	"context"
	"fmt"

	"gitlab.com/golight/dao"
	"gitlab.com/golight/dao/params"
	"gitlab.com/golight/dao/types"
	"gitlab.com/k4ktys/quote_service/modules/quote/entity"

	"time"
)

type QuotesStorage struct {
	adapter dao.DAOFace
}

func NewQuotesStorage(sqlAdapter dao.DAOFace) QuotesStorage {
	return QuotesStorage{adapter: sqlAdapter}
}

func (q *QuotesStorage) Create(ctx context.Context, dto entity.Quote) (int64, error) {
	return q.adapter.Create(ctx, &dto)
}

func (q *QuotesStorage) Update(ctx context.Context, dto entity.Quote) error {
	return q.adapter.Update(
		ctx,
		&dto,
		params.Condition{
			Equal: map[string]interface{}{"id": dto.ID},
		},
		params.Update,
	)
}

func (q *QuotesStorage) GetByID(ctx context.Context, quotesID int) (entity.Quote, error) {
	var list []entity.Quote
	var table entity.Quote
	err := q.adapter.List(ctx, &list, &table, params.Condition{
		Equal: map[string]interface{}{"id": quotesID},
	})
	if err != nil {
		return entity.Quote{}, err
	}
	if len(list) < 1 {
		return entity.Quote{}, fmt.Errorf("quotes storage: GetByID not found")
	}
	return list[0], err
}

func (q *QuotesStorage) GetList(ctx context.Context, condition params.Condition) ([]entity.Quote, error) {
	var list []entity.Quote
	var table entity.Quote
	err := q.adapter.List(ctx, &list, &table, condition)
	if err != nil {
		return nil, err
	}

	return list, nil
}

func (q *QuotesStorage) Delete(ctx context.Context, quotesID int) error {
	table, err := q.GetByID(ctx, quotesID)
	if err != nil {
		return err
	}

	table.DeletedAt = types.NewNullTime(time.Now())

	return q.adapter.Update(
		ctx,
		&table,
		params.Condition{
			Equal: map[string]interface{}{"id": table.ID},
		},
		params.Update,
	)
}
