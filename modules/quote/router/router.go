package router

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/k4ktys/quote_service/modules/quote/handler"
)

func QuoteRouter(handler handler.QuoteHandlerer) http.Handler {
	r := chi.NewRouter()

	r.Get("/get_random_quote", handler.GetRandomQuote)

	return r
}
