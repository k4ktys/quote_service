package handler

import (
	"net/http"

	"gitlab.com/golight/responder"
	"gitlab.com/k4ktys/quote_service/modules/quote/service"
)

type QuoteHandlerer interface {
	GetRandomQuote(w http.ResponseWriter, r *http.Request)
}

type QuoteHandler struct {
	responder responder.Responder
	service   service.QuoteServicer
}

func NewQuoteHandler(r responder.Responder, s service.QuoteServicer) QuoteHandlerer {
	return &QuoteHandler{
		responder: r,
		service:   s,
	}
}

func (q *QuoteHandler) GetRandomQuote(w http.ResponseWriter, r *http.Request) {
	response, err := q.service.GetRandomQuote(r.Context())

	if err != nil {
		q.responder.ErrorInternal(w, err)
		return
	}

	q.responder.OutputJSON(w, response)
}
