package service

import (
	"context"
	"math/rand"
	"time"

	"gitlab.com/golight/dao/params"
	"gitlab.com/k4ktys/quote_service/modules/quote/entity"
	"gitlab.com/k4ktys/quote_service/modules/quote/repository"
)

type QuoteServicer interface {
	GetRandomQuote(ctx context.Context) (entity.QuoteDTO, error)
}

type QuoteService struct {
	repository repository.Quoteser
	rand       *rand.Rand
}

func NewQuoteService(r repository.Quoteser) QuoteServicer {
	s := rand.NewSource(time.Now().Unix())
	rand := rand.New(s)

	return &QuoteService{
		repository: r,
		rand:       rand,
	}
}

func (q *QuoteService) GetRandomQuote(ctx context.Context) (entity.QuoteDTO, error) {
	var response entity.QuoteDTO

	quotes, err := q.repository.GetList(ctx, params.Condition{})

	if err != nil {
		return response, err
	}

	randomQuote := quotes[rand.Intn(len(quotes))]

	response = entity.QuoteDTO{
		ID:     randomQuote.ID,
		Text:   randomQuote.Text,
		Author: randomQuote.Author,
	}

	return response, nil
}
