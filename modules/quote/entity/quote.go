package entity

import (
	"time"

	"gitlab.com/golight/dao/types"
)

type QuoteDTO struct {
	ID     int    `json:"id"`
	Text   string `json:"text"`
	Author string `json:"author"`
}

//go:generate genstorage -filename=$GOFILE
type Quote struct {
	ID        int            `json:"-" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null" db_ops:"id"`
	Text      string         `json:"-" db:"text" db_ops:"create,conflict,upsert" db_type:"varchar(512)" db_default:"not null" db_index:"index,unique"`
	Author    string         `json:"-" db:"author" db_ops:"create,update,upsert" db_type:"varchar(144)" db_default:"not null"`
	CreatedAt time.Time      `json:"-" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	UpdatedAt time.Time      `json:"-" db:"updated_at" db_ops:"update" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	DeletedAt types.NullTime `json:"-" db:"deleted_at" db_ops:"update" db_type:"timestamp" db_default:"default null" db_index:"index"`
}

func (q *Quote) TableName() string {
	return "quotes"
}

func (q *Quote) OnCreate() []string {
	return []string{}
}

func (q *Quote) FieldsPointers() []interface{} {
	return []interface{}{
		&q.ID,
		&q.Text,
		&q.Author,
		&q.CreatedAt,
		&q.UpdatedAt,
		&q.DeletedAt,
	}
}
