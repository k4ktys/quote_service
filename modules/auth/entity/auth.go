package entity

type AuthDTO struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}
