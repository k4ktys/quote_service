package handler

import (
	"encoding/json"
	"net/http"
	"time"

	"gitlab.com/golight/responder"
	"gitlab.com/k4ktys/quote_service/modules/auth/entity"
	"gitlab.com/k4ktys/quote_service/modules/auth/service"
)

type AuthHandlerer interface {
	Register(w http.ResponseWriter, r *http.Request)
	Login(w http.ResponseWriter, r *http.Request)
}

type AuthHandler struct {
	responder responder.Responder
	service   service.AuthServicer
}

func NewAuthHandler(r responder.Responder, s service.AuthServicer) AuthHandlerer {
	return &AuthHandler{
		responder: r,
		service:   s,
	}
}

func (u *AuthHandler) Login(w http.ResponseWriter, r *http.Request) {
	var authReq entity.AuthDTO

	err := json.NewDecoder(r.Body).Decode(&authReq)
	if err != nil {
		u.responder.ErrorBadRequest(w, err)
		return
	}

	authToken, err := u.service.Login(r.Context(), authReq)

	if err != nil {
		u.responder.ErrorInternal(w, err)
		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:    "jwt",
		Value:   authToken,
		Expires: time.Now().Add(1 * time.Minute),
		Path:    "/",
	})

	u.responder.OutputJSON(w, authToken)
}

func (u *AuthHandler) Register(w http.ResponseWriter, r *http.Request) {
	var authReq entity.AuthDTO

	err := json.NewDecoder(r.Body).Decode(&authReq)
	if err != nil {
		u.responder.ErrorBadRequest(w, err)
		return
	}

	authToken, err := u.service.Register(r.Context(), authReq)

	if err != nil {
		u.responder.ErrorInternal(w, err)
		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:    "jwt",
		Value:   authToken,
		Expires: time.Now().Add(1 * time.Minute),
		Path:    "/",
	})

	u.responder.OutputJSON(w, authToken)
}
