package service

import (
	"context"
	"fmt"
	"time"

	"github.com/go-chi/jwtauth/v5"
	"gitlab.com/k4ktys/quote_service/modules/auth/entity"
	userEntity "gitlab.com/k4ktys/quote_service/modules/user/entity"
	"gitlab.com/k4ktys/quote_service/modules/user/service"
	"golang.org/x/crypto/bcrypt"
)

var tokenAuth *jwtauth.JWTAuth

type AuthServicer interface {
	Login(ctx context.Context, authDto entity.AuthDTO) (string, error)
	Register(ctx context.Context, authDto entity.AuthDTO) (string, error)
}

type AuthService struct {
	userService service.UserServicer
}

func NewAuthService(us service.UserServicer) AuthServicer {
	tokenAuth = jwtauth.New("HS256", []byte("my_secret_key"), nil)

	return &AuthService{
		userService: us,
	}
}

func (a *AuthService) Login(ctx context.Context, authDto entity.AuthDTO) (string, error) {
	user, err := a.userService.GetByLogin(ctx, authDto.Login)

	if err != nil {
		return "", err
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.PasswordHash), []byte(authDto.Password))

	if err != nil {
		return "", err
	}

	expirationTime := time.Now().Add(1 * time.Minute)

	claims := map[string]interface{}{
		"login": authDto.Login,
	}

	jwtauth.SetExpiry(claims, expirationTime)

	_, tokenString, err := tokenAuth.Encode(claims)

	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func (a *AuthService) Register(ctx context.Context, authDto entity.AuthDTO) (string, error) {
	if _, err := a.userService.GetByLogin(ctx, authDto.Login); err == nil {
		return "", fmt.Errorf("user: %s is already registered", authDto.Login)
	}

	expirationTime := time.Now().Add(1 * time.Minute)

	claims := map[string]interface{}{
		"login": authDto.Login,
	}

	jwtauth.SetExpiry(claims, expirationTime)

	_, tokenString, err := tokenAuth.Encode(claims)

	if err != nil {
		return "", err
	}

	passwordHash, err := getHashPassword(authDto.Password)
	if err != nil {
		return "", err
	}

	_, err = a.userService.Create(ctx, userEntity.UserDTO{
		Login:        authDto.Login,
		PasswordHash: passwordHash,
	})

	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func getHashPassword(password string) (string, error) {
	bytePassword := []byte(password)

	hash, err := bcrypt.GenerateFromPassword(bytePassword, bcrypt.DefaultCost)

	if err != nil {
		return "", err
	}

	return string(hash), nil
}
