package router

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/k4ktys/quote_service/modules/auth/handler"
)

func AuthRouter(handler handler.AuthHandlerer) http.Handler {
	r := chi.NewRouter()

	r.Post("/login", handler.Login)
	r.Post("/register", handler.Register)

	return r
}
