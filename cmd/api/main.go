package main

import (
	"context"
	"fmt"
	"log"

	"github.com/jmoiron/sqlx"
	"github.com/ptflp/godecoder"
	"gitlab.com/golight/dao"
	"gitlab.com/golight/dao/params"
	"gitlab.com/golight/loggerx"
	"gitlab.com/golight/migrator"
	"gitlab.com/golight/responder"
	"gitlab.com/golight/scanner"
	"gitlab.com/golight/server"
	"gitlab.com/k4ktys/quote_service/config"
	aHandler "gitlab.com/k4ktys/quote_service/modules/auth/handler"
	aService "gitlab.com/k4ktys/quote_service/modules/auth/service"
	quoteEntity "gitlab.com/k4ktys/quote_service/modules/quote/entity"
	qHandler "gitlab.com/k4ktys/quote_service/modules/quote/handler"
	qRepo "gitlab.com/k4ktys/quote_service/modules/quote/repository"
	qService "gitlab.com/k4ktys/quote_service/modules/quote/service"
	userEntity "gitlab.com/k4ktys/quote_service/modules/user/entity"
	uHandler "gitlab.com/k4ktys/quote_service/modules/user/handler"
	uRepo "gitlab.com/k4ktys/quote_service/modules/user/repository"
	uService "gitlab.com/k4ktys/quote_service/modules/user/service"
	"gitlab.com/k4ktys/quote_service/router"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"

	_ "github.com/lib/pq"
)

func main() {
	conf, err := config.NewConfig()
	if err != nil {
		log.Fatal(err)
	}

	logger := loggerx.InitLogger(conf.AppName, conf.Production)

	decoder := godecoder.NewDecoder()
	responder := responder.NewResponder(decoder, logger)

	ts := scanner.NewTableScanner()
	ts.RegisterTable(&quoteEntity.Quote{}, &userEntity.User{})

	dbConnection := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable", conf.DbUser, conf.DbPassword, conf.DbName)
	db, err := sqlx.Connect(conf.DbDriver, dbConnection)

	if err != nil {
		logger.Fatal("failed db connection", zap.Error(err))
	}

	d := dao.NewDAO(db, params.DB{Driver: conf.DbDriver}, ts)

	err = migrator.NewMigrator(db, params.DB{Driver: conf.DbDriver}, ts).Migrate()
	if err != nil {
		logger.Fatal("failed to migrate", zap.Error(err))
	}

	d.Create(context.Background(), &quoteEntity.Quote{Text: "1. A lot of very inspiring text", Author: "Author 1"})
	d.Create(context.Background(), &quoteEntity.Quote{Text: "2. A lot of very inspiring text", Author: "Author 2"})
	d.Create(context.Background(), &quoteEntity.Quote{Text: "3. A lot of very inspiring text", Author: "Author 3"})
	d.Create(context.Background(), &quoteEntity.Quote{Text: "4. A lot of very inspiring text", Author: "Author 4"})
	d.Create(context.Background(), &quoteEntity.Quote{Text: "5. A lot of very inspiring text", Author: "Author 5"})
	d.Create(context.Background(), &quoteEntity.Quote{Text: "6. A lot of very inspiring text", Author: "Author 6"})

	userRepository := uRepo.NewUsersStorage(d)
	userService := uService.NewUserService(&userRepository)
	userHandler := uHandler.NewUserHandler(responder, userService)

	quoteRepository := qRepo.NewQuotesStorage(d)
	quoteService := qService.NewQuoteService(&quoteRepository)
	quoteHandler := qHandler.NewQuoteHandler(responder, quoteService)

	authService := aService.NewAuthService(userService)
	authHandler := aHandler.NewAuthHandler(responder, authService)

	r := router.NewRouter(userHandler, quoteHandler, authHandler)

	srv := server.NewHTTPServer(logger, server.WithHTTPHandler(r), server.WithConfig(server.Config{
		Port: conf.Port,
	}))

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	errGrp, ctx := errgroup.WithContext(ctx)

	errGrp.Go(func() error {
		return server.HandleSignals(ctx, cancel)
	})

	errGrp.Go(func() error {
		return srv.Serve(ctx)
	})

	if err := errGrp.Wait(); err != nil {
		logger.Error("server error", zap.Error(err))
	}
}
