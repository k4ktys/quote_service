package config

import (
	"github.com/ilyakaznacheev/cleanenv"
	"github.com/joho/godotenv"
)

type Config struct {
	AppName    string `env:"APP_NAME" env-default:"quote_service"`
	Production bool   `env:"PRODUCTION" env-default:"false"`

	Port string `env:"PORT" env-default:"8080"`

	DbDriver   string `env:"DB_DRIVER"`
	DbUser     string `env:"DB_USER"`
	DbPassword string `env:"DB_PASSWORD"`
	DbPort     string `env:"DB_PORT"`
	DbName     string `env:"DB_NAME"`

	JwtKey string `env:"JWT_KEY" env-default:"my_secret_key"`
}

func NewConfig() (*Config, error) {
	config := &Config{}

	err := godotenv.Load()
	if err != nil {
		return nil, err
	}

	err = cleanenv.ReadEnv(config)
	if err != nil {
		return nil, err
	}

	return config, nil
}
