package router

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth/v5"
	aHandler "gitlab.com/k4ktys/quote_service/modules/auth/handler"
	aRouter "gitlab.com/k4ktys/quote_service/modules/auth/router"
	qHandler "gitlab.com/k4ktys/quote_service/modules/quote/handler"
	qRouter "gitlab.com/k4ktys/quote_service/modules/quote/router"
	uHandler "gitlab.com/k4ktys/quote_service/modules/user/handler"
	uRouter "gitlab.com/k4ktys/quote_service/modules/user/router"
)

func NewRouter(userHandler uHandler.UserHandlerer, quoteHandler qHandler.QuoteHandlerer, authHandler aHandler.AuthHandlerer) *chi.Mux {
	//нельзя так ключ делать
	tokenAuth := jwtauth.New("HS256", []byte("my_secret_key"), nil)

	r := chi.NewRouter()

	r.Route("/api/v1", func(r chi.Router) {
		r.Mount("/auth", aRouter.AuthRouter(authHandler))
		r.Mount("/user", uRouter.UserRouter(userHandler))

		r.Group(func(r chi.Router) {
			r.Use(jwtauth.Verifier(tokenAuth))
			r.Use(jwtauth.Authenticator(tokenAuth))

			r.Mount("/quote", qRouter.QuoteRouter(quoteHandler))
		})
	})

	return r
}
